package ru.maksaimer.s05;

import java.util.Collections;

public class Hero {
    private static final int INITIAL_HEALTH = 100;

    protected int health;
    private final int power;
    private final int speed;

    public Hero() {
        this.health = INITIAL_HEALTH;
        this.power = 50;
        this.speed = 50;
    }

    public Hero(int power) {
        this.health = INITIAL_HEALTH;
        this.power = power % 100;
        this.speed = 100 - this.power;
    }

    public int hit(Hero hero) {
        hero.health -= this.power;

        return this.power;
    }

    @Override
    public String toString() {
        return "health \t" + repeatAsterisk(health / 10) + "\n" +
                "power \t" + repeatAsterisk(power / 10) + "\n" +
                "speed \t" + repeatAsterisk(speed / 10) + "\n";
    }

    private static String repeatAsterisk(int times) {
        return String.join("", Collections.nCopies(times, "*"));
    }

    public int getHealth() {
        return health;
    }

    public int getPower() {
        return power;
    }

    public int getSpeed() {
        return speed;
    }
}