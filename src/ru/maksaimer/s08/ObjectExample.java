package ru.maksaimer.s08;

public class ObjectExample {
    // 1 - Файл существует, в нем присутствует первая строка
    // 2 - Файл существует, но в нем нет первой строки
    // 3 - Файл не существует

    static class FirstLineResult {
        String firstLine;
        boolean isExists;

        public FirstLineResult(String firstLine, boolean isExists) {
            this.firstLine = firstLine;
            this.isExists = isExists;
        }
    }

    public static void main(String[] args) {
        FirstLineResult result = readFirstLine("path/to/file");
        if (result.firstLine != null) {
            // 1
        } else if (result.firstLine == null && result.isExists) {
            // 2
        } else {
            // 3;
        }
    }

    static FirstLineResult readFirstLine(String path) {
        return new FirstLineResult("", false);
    }
}
