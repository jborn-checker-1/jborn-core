package ru.maksaimer.s08;

public class StacktraceSimple {
    public static void main(String[] args) {
        sendIfEven(2, null);
    }

    private static void sendIfEven(int i, String message) {
        if (i % 2 == 0) {
            sendMessage(message);
        }
    }

    public static void sendMessage(String message) {
        if (message == null) {
            throw new IllegalArgumentException("Message can not be null!");
        }
        // код для отправки сообщения
    }
}
