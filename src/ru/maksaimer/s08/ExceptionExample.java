package ru.maksaimer.s08;

import java.io.FileNotFoundException;

public class ExceptionExample {
    // 1 - Файл существует, в нем присутствует первая строка
    // 2 - Файл существует, но в нем нет первой строки
    // 3 - Файл не существует

    public static void main(String[] args) {
        try {
            String result = readFirstLine("path/to/file");
            if (result != null) {
                // 1
            } else {
                // 2
            }
        } catch (FileNotFoundException e) {
            // 3
        }
    }

    static String readFirstLine(String path)throws FileNotFoundException {
        return "";
    }
}
