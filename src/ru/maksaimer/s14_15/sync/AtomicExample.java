package ru.maksaimer.s14_15.sync;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicExample extends Thread {
    static AtomicInteger count = new AtomicInteger(0);

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            count.incrementAndGet();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new AtomicExample();
        Thread t2 = new AtomicExample();

        t1.start();
        t2.start();

        t1.join();
        t2.join();

        System.out.println("State = " + count.get());
    }
}
