package ru.maksaimer.s14_15.sync;

public class SyncIncrementThread implements Runnable {
    private static int state = 0;

    private /*synchronized*/ static void inc() {
        state++;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100000; i++) {
            inc();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new SyncIncrementThread());
        Thread t2 = new Thread(new SyncIncrementThread());

        t1.start();
        t2.start();

        t1.join();
        t2.join();

        System.out.println("State = " + state);
    }
}
