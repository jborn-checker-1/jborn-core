package ru.maksaimer.s14_15;

public class CurrentThreadInfo {

    public static void main(String[] args) {
        Thread thread = Thread.currentThread();

        System.out.println("Thread name: " + thread.getName());
        System.out.println("Thread priority: " + thread.getPriority());
        System.out.println("Thread group name: " + thread.getThreadGroup().getName());

        System.out.println();

        System.out.println("Thread info: " + thread);

        thread.setPriority(1);
        thread.setName("My Thread");

        System.out.println("Thread info after change: " + thread);
    }
}
