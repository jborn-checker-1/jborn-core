package ru.maksaimer.s14_15.create_thread;

public class RunnableExample implements Runnable {
    @Override
    @SuppressWarnings("Duplicates")
    public void run() {
        System.out.println(Thread.currentThread().getName() + " start");

        try {
            for (int i = 0; i < 5; i++) {
                System.out.println(Thread.currentThread().getName() + " " + i);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(Thread.currentThread().getName() + " finish");
    }

    public static void main(String[] args) {
        RunnableExample runnable = new RunnableExample();
        Thread thread = new Thread(runnable);
        thread.start();
    }
}
