package ru.maksaimer.s06;

public class BlasterTank extends AbstractTank {
    private int shield;
    private int plasmaDamage;
    private int energy;

    @Override
    public int hit(Hero enemy) {
        if (energy >= plasmaDamage) {
            enemy.health -= plasmaDamage;
            energy -= plasmaDamage;
        }

        return enemy.health;
    }

    @Override
    public void takeAmmo(AbstractTank tank, int ammoPoints) {

    }
}
