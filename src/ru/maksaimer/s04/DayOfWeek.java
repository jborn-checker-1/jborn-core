package ru.maksaimer.s04;

public enum DayOfWeek {
    MONDAY(false),
    TUESDAY(false),
    WEDNESDAY(false),
    THURSDAY(false),
    FRIDAY(false),
    SATURDAY(true),
    SUNDAY(true);

    private boolean isDayOff;

    DayOfWeek(boolean isDayOff) {
        this.isDayOff = isDayOff;
    }

    public boolean isDayOff() {
        return isDayOff;
    }

    public int dayNumber() {
        return ordinal() + 1;
    }
}
