package ru.maksaimer.s09;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainCopyFile {
    public static void main(String[] args) {
        try (FileInputStream fis = new FileInputStream("/home/maksaimer/jborn/JBorn_Core_07_Maksaimer/example.txt");
             FileOutputStream fos = new FileOutputStream("/home/maksaimer/jborn/JBorn_Core_07_Maksaimer/example_copy.txt");) {
            int b;
            while ((b = fis.read()) > 0) {
                fos.write(b);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
