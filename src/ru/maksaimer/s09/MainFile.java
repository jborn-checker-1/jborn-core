package ru.maksaimer.s09;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class MainFile {
    public static void main(String[] args) throws IOException {
        Path path = Paths.get("/home/maksaimer/Projects");
        DirectoryStream<Path> paths = Files.newDirectoryStream(path);
        for (Path p1 : paths) {
            System.out.println(p1);
        }


    }

    public static StringBuffer find(File path, String stringForSearch) {
        StringBuffer sb = new StringBuffer();

        for (File item : path.listFiles()) {
            if (item.isDirectory()) {
                if (item.getName().contains(stringForSearch)) {
                    sb.append(item.getAbsolutePath())
                      .append("\n");
                }

                sb.append(find(item, stringForSearch));
            } else if (item.getName().contains(stringForSearch)) {
                sb.append(item.getAbsolutePath())
                  .append("\n");
            }
        }

        return sb;
    }

    public static String requestStringForSearch() {
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите строку для поиска: ");
        return scan.next();
    }
}