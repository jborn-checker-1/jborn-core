package ru.maksaimer.s10;

public class CustomLinkedList<E> {

    private static class Node<T> {
        T value;
        Node<T> prev;
        Node<T> next;

        Node(T value) {
            this.value = value;
        }
    }

    int size;
    Node<E> first;
    Node<E> last;

    public void add(E element) {
        Node<E> node = new Node<>(element);

        if (first == null) {
            first = node;
            last = node;
        } else {
            last.next = node;
            node.prev = last;

            last = node;
        }

        size++;
    }

    public E get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }

        int i = 0;
        Node<E> current = first;
        while (i != index) {
            current = current.next;
            i++;
        }

        return current.value;
    }

    public int size() {
        return size;
    }

    public static void main(String[] args) {
        CustomLinkedList<Integer> list = new CustomLinkedList<>();

        for (int i = 0; i < 10; i++) {
            list.add(i);
        }

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }
}
